// main.rs
//
// Copyright 2019 Alberto Ruiz <aruiz@redhat.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::env;
use std::process;

mod blenv;
mod cmdline;
mod bls;

use cmdline::CmdlineHandler;
use bls::BLSEntry;

const ENV_CLAMP: Option<usize> = None;

/* Usage output */
fn env_usage() {
  eprintln!("  env show                     # outputs all parameters and its values");
  eprintln!("  env get   PARAM              # outputs the value of a blsenv parameter");
  eprintln!("  env set   PARAM VALUE        # sets the value of a blsenv parameter replacing previously existing values");
  eprintln!("  env unset PARAM              # removes parameter from the environment");
}

fn cmdline_usage() {
  eprintln!("  cmdline ENTRY|env show                     # shows the kernel cmdline from a bls ENTRY (options), for all entries or for the entire environment ($kernelopts)");
  eprintln!("  cmdline ENTRY|env get PARAM                # displays the value of parameter 'param'");
  eprintln!("  cmdline ENTRY|all|env set PARAM[=VALUE]    # sets pairs of 'params' to corresponding'value', you can also set a parameter with no value");
  eprintln!("                                             # does not work when multiple values are set with `env add`");
  eprintln!("  cmdline ENTRY|all|env add PARAM[=VALUE]    # adds an additional 'param' or 'param=value' pair to the cmdline even if it was previously set");
  eprintln!("  cmdline ENTRY|all|env remove PARAM[=VALUE] # removes a specific param or param=value pair from the cmdline, to remove all pairs or");
  eprintln!("                                             # a parameter with no value use 'clear'");
  eprintln!("  cmdline ENTRY|all|env clear PARAM          # removes all instances of a specific parameter");
}

fn entry_usage() {
  eprintln!("  entry list                   # lists all Boot Loader Spec *.conf entries in order");
  eprintln!("  entry default [ENTRY]        # shows the default BLS entry to boot into or sets it if an argument is given,");
}

fn help_usage() {
  eprintln!("  help [COMMAND]               # prints this usage help or help for specific command");
}

fn log_usage () {
  eprintln!("blsfg usage:");
  env_usage();
  eprintln!("");
  cmdline_usage();
  eprintln!("");
  entry_usage();
  help_usage();
}

fn assert_invalid_command(expected_length: usize, args: &[String], max: bool, usage: &Fn()) {
  if args.len() < expected_length {
    eprintln!("ERROR insuficient arguments: `{}`", args.join(" "));
  } else if max && args.len() > expected_length {
    eprintln!("ERROR too many arguments: `{}`", args.join(" "));
  } else {
    return;
  }
  usage();
  process::exit(1);
}

fn handle_env (args: &[String]) {
  assert_invalid_command(2, args, false, &env_usage);
  let env = match blenv::Environment::new(ENV_CLAMP, &blenv::get_env_path()) {
    Ok(env) => env,
    Err(e) => {
      eprintln!("ERROR: Could not open grubenv: {}", e);
      return;
    }
  };

  match args[1].as_str() {
    "get" => {
      assert_invalid_command(3, args, true, &env_usage);
      match env.get(&args[2]) {
        Ok(val) => {
          println!("{}", val);
        }
        Err(error) => {
          eprintln!("{}", error);
          process::exit(1);
        }
      }
    }
    "set" => {
      assert_invalid_command(4, args, true, &env_usage);
      match env.set(&args[2], &args[3]) {
        Ok(_) => {},
        Err(error) => {
          eprintln!("{}", error);
          process::exit(1);
        }
      };
    },
    "unset" => {
      assert_invalid_command(3, args, true, &env_usage);
      match env.unset(&args[2]) {
        Ok(_) => {},
        Err(error) => {
          eprintln!("{}", error);
          process::exit(1);
        }
      };
    }
    "show" => {
      assert_invalid_command(2, args, true, &env_usage);
      match env.show() {
        Ok(output) => { println!("{}", output)},
        Err(error) => {
          eprintln!("{}", error);
          process::exit(1);
        }
      }
    }
    _ => {
      eprintln!("ERROR: unrecognized command {}", args.join(" "));
      process::exit(1);
    }
  }
}

fn handle_cmdline(args: &[String]) {
  let base: usize = 3;
  assert_invalid_command(base, args, false, &cmdline_usage);

  let mut env = match blenv::Environment::new(ENV_CLAMP, &blenv::get_env_path()) {
    Ok(env) => Some(env),
    Err(e) => {
      eprintln!("ERROR: Could not open grubenv: {}", e);
      None
    }
  };

  let mut _entry;
  let mut entries = BLSEntry::get_bls_entries().unwrap_or_else (|e| {
        eprintln!("ERROR: {}", e);
        process::exit(1);
      });
  let provider: &mut CmdlineHandler = match &args[base-2] {
    entry if entry.as_str() == "all" => {
      match args[base-1].as_str() {
        arg @ "get" | arg @ "show" => {
          eprintln!("ERROR: cannot {} cmdline for all entries", arg);
          process::exit(1);
        }
        _ => {}
      };
      &mut entries
    },
    entry if entry.as_str() == "env" => {
      match env.as_mut() {
        Some(env) => env,
        None => {
          return;
        }
      }
    },
    entry @ _ => {
      let blsentry = if entry == "default" {
        let env = match env.as_mut() {
          Some(env) => env,
          None => {
            return;
          }
        };
        match env.get("saved_entry") {
          Ok(entry) => BLSEntry::new(&entry),
          _ => BLSEntry::new(entry)
        }
      } else {
        BLSEntry::new(entry)
      };
      match blsentry {
        Ok(ent) => { _entry = ent; &mut _entry }
        Err(_) => {
          eprintln!("ERROR: {} is not a valid bootloader entry", entry);
          process::exit(1);
        }
      }
    }
  };

  macro_rules! exit_error {
    () => { |msg| {
        eprintln!("ERROR: {}", msg);
        process::exit(1);
      }
    }
  };

  match args[base-1].as_str() {
    "show" => {
      assert_invalid_command(base, args, true, &cmdline_usage);
      println!("{}", provider.cmdline_render()
                      .unwrap_or_else(exit_error!()));
    },
    "get" => {
      assert_invalid_command(base+1, args, true, &cmdline_usage);
      match provider.cmdline_get(&args[base]) {
        Ok(params) => {
          for param in params {
            print!("{}", &args[base]);
            if let Some(param) = param  {
              print!("={}", param);
            }
            println!("");
          }
        },
        Err(error) => {
          eprintln!("ERROR: {}", error);
          process::exit(1);
        }
      }
    },
    "set" => {
      assert_invalid_command(base+1, args, false, &cmdline_usage);
      match provider.cmdline_set(&args[base..]) {
        Ok(()) => {},
        Err(error) => {
          eprintln!("ERROR: {}", error);
          process::exit(1);
        }
      }
    },
    "add" => {
      assert_invalid_command(base+1, args, false, &cmdline_usage);
      match provider.cmdline_add(&args[base..]) {
        Ok(()) => {},
        Err(error) => {
          eprintln!("ERROR: {}", error);
          process::exit(1);
        }
      }
    },
    "remove" => {
      assert_invalid_command(base+1, args, false, &cmdline_usage);
      match provider.cmdline_remove(&args[base..]) {
        Ok(()) => {},
        Err(error) => {
          eprintln!("ERROR: {}", error);
          process::exit(1);
        }
      }
    },
    "clear" => {
      assert_invalid_command(base+1, args, false, &cmdline_usage);
      match provider.cmdline_clear(&args[base..]) {
        Ok(()) => {},
        Err(error) => {
          eprintln!("ERROR: {}", error);
          process::exit(1);
        }
      }
    },
    _ => {
      eprintln!("ERROR: unrecognized command {}", args.join(" "));
      process::exit(1);
    }
  }
  process::exit(0);
}

fn handle_entry(args: &[String]) {
  assert_invalid_command(2, args, false, &entry_usage);
  match args[1].as_str() {
    "list" => {
      assert_invalid_command(2, args, true, &entry_usage);
      let entries = BLSEntry::get_bls_entries ()
              .unwrap_or_else(|e| { eprintln!("ERROR: could not read bootloader entries from directory {}", e);
                                    process::exit(1); });
      let mut index: u8 = 0;
      for entry in entries {
        println!("{} {}", index, entry);
        index += 1;
      }
    }
    "default" => {
      let mut env = match blenv::Environment::new(ENV_CLAMP, &blenv::get_env_path()) {
        Ok(env) => env,
        Err(e) => {
          eprintln!("ERROR: Could not open grubenv: {}", e);
          return;
        }
      };
      assert_invalid_command(2, args, false, &entry_usage);
      match args.len() {
        2 => {
          match env.get("saved_entry") {
            Ok(entry) => {
              let index = match entry.parse::<usize>() {
                Ok(val) => val,
                _ => {
                  println!("{}", &entry);
                  process::exit(0);
                }
              };

              println!("{}", BLSEntry::get_bls_entries()
                .unwrap_or_else(|e| { eprintln!("ERROR: could not read bootloader entries from directory {}", e);
                                      process::exit(1); })
                .remove(index));
              process::exit(0);
            },
            Err(error) => {
              eprintln!("ERROR: {}", error);
              process::exit(1);
            }
          }
        },
        3 => {
          match BLSEntry::new(&args[2]) {
            //FIXME: Check if entry is valid
            Ok(entry) => {
              match env.set("saved_entry", entry.name.as_str()) {
                Ok(_) => {},
                Err(error) => {
                  eprintln!("ERROR: {}", error);
                  process::exit(1);
                }
              }
            },
            Err(error) => {
              eprintln!("ERROR: {}", error);
              process::exit(1);
            }
          }
        },
        _ => {}
      }
    },
    _ => {}
  }
}

fn handle_help(args: &[String]) {
  assert_invalid_command(1, &args[1..], true, &help_usage);
  log_usage();
}

fn main() {
    let args: Vec<_> = env::args().collect();
    if args.len() <= 1 {
      log_usage();
      process::exit(1);
    }

    match args[1].as_str() {
      "env"     => handle_env(&args[1..]),
      "cmdline" => handle_cmdline(&args[1..]),
      "entry"   => handle_entry(&args[1..]),
      "help"    => handle_help(&args[1..]),
      _         => {
        eprintln!("ERROR: invalid command {}", args[1]);
        log_usage();
        process::exit(1);
      }
    }
    process::exit(0);
}
