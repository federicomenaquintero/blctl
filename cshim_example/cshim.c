/* cshim.c
 *
 * Copyright 2019 Alberto Ruiz <aruiz@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdio.h>
#include <stdlib.h>

#include "../include/blctl.h"

void print_and_free(char* str) {
  printf("%s\n", str);
  free(str);
}

void cmdline_print (const char* value, void* user_data) {
  printf("%s", (const char*)user_data);
  if (value) printf("=%s", value);
  printf("\n");
}

int main () {
  BLSEnv* env = bls_env_new(0, "/boot/efi/EFI/fedora/grubenv");
  if (!env) {
    printf("Could not create environment object\n");
    return 1;
  }

  if (bls_env_set(env, "foo", "bar")) {
    printf("Could set foo=bar\n");
    print_and_free (bls_env_get(env, "foo"));
    printf(bls_env_unset(env, "foo") ? "Could unset foo\n" : "Could not unset foo\n");
  } else {
    printf("Could not set foo=bar\n");
  }
  print_and_free (bls_env_get(env, "kernelopts"));
  print_and_free (bls_env_show(env));

  const char* params[] = {"foo=bar", "foo", "bar=2", 0};
  const char* params_add[] = {"foo=4", 0};
  const char* params_rem[] = {"foo", 0};
  const char* params_clear[] = {"foo", "bar", 0};

  BLSCmdline env_cmdline = bls_env_to_cmdline(env);

  if (bls_cmdline_add(env_cmdline, params)) {
    printf("Could set cmdline params\n");
  } else {
    printf("Could not set cmdline params\n");
  }

  BLSCmdlineParam* foo = bls_cmdline_get(env_cmdline, "foo");
  if (!foo) {
    printf("Could not get cmdline param foo\n");
  } else {
    printf("Cmdline param foo:\n");
    bls_cmdline_param_foreach(foo, &cmdline_print, "foo");
    bls_cmdline_param_destroy(foo);
  }
  print_and_free (bls_env_get(env, "kernelopts"));

  if (!bls_cmdline_remove(env_cmdline, params_rem)) {
    printf("Could not remove cmdline param 'foo'\n");
  } else {
    printf("Removed cmdline param 'foo'\n");
  }
  print_and_free (bls_env_get(env, "kernelopts"));

  if (!bls_cmdline_clear(env_cmdline, params_clear)) {
    printf("Could not clear cmdline params 'foo' & 'bar'\n");
  } else {
    printf("Cleared cmdline params 'foo' & 'bar'\n");
  }
  print_and_free (bls_env_get(env, "kernelopts"));

  bls_env_destroy(env);

  // BLSEntry
  BLSEntry* entry = bls_entry_new("26e796747b524601a8faa251403a37ab-5.0.0-300.fc30.x86_64");
  if (!entry) {
      printf("Could not create BLS entry\n");
      return 1;
  }
  BLSCmdline entry_cmdline = bls_entry_to_cmdline(entry);

  if (bls_cmdline_add(entry_cmdline, params)) {
    printf("Could set entry cmdline params\n");
  } else {
    printf("Could not set entry cmdline params\n");
  }

  foo = bls_cmdline_get(entry_cmdline, "foo");
  if (!foo) {
    printf("Could not get cmdline param foo\n");
  } else {
    printf("Cmdline param foo:\n");
    bls_cmdline_param_foreach(foo, &cmdline_print, "foo");
    bls_cmdline_param_destroy(foo);
  }

  if (!bls_cmdline_remove(entry_cmdline, params_rem)) {
    printf("Could not remove cmdline param 'foo'\n");
  } else {
    printf("Removed cmdline param 'foo'\n");
  }

  foo = bls_cmdline_get(entry_cmdline, "foo");
  if (!foo) {
    printf("Could not get cmdline param foo\n");
  } else {
    printf("Cmdline param foo:\n");
    bls_cmdline_param_foreach(foo, &cmdline_print, "foo");
    bls_cmdline_param_destroy(foo);
  }

  if (!bls_cmdline_clear(entry_cmdline, params_clear)) {
    printf("Could not clear cmdline params 'foo' & 'bar'\n");
  } else {
    printf("Cleared cmdline params 'foo' & 'bar'\n");
  }

  printf(bls_entry_set(entry, "mycommand", "myvalue")? "Set BLS entry command mycommand=myvalue\n" : "Could not set BLS entry command 'mycommand'\n");
  char *value = bls_entry_get(entry, "mycommand");
  printf("mycommand=%s\n", value);
  free(value);

  printf(bls_entry_remove(entry, "mycommand")? "Removed BLS entry command 'mycommand'\n" : "Failed to remove BLS command 'mycommand'\n");

  bls_entry_destroy(entry);

  BLSEntryList* all = bls_entry_list();
  if (!entry) {
      printf("Could not create BLS entry list\n");
      return 1;
  }
  BLSCmdline all_cmdline = bls_entry_list_to_cmdline (all);

  if (bls_cmdline_set (all_cmdline, params)) {
    printf("Params could be set to all entries\n");
  } else {
    printf("Params could NOT be set to all entries\n");
  }

  if (bls_cmdline_add (all_cmdline, params_add)) {
    printf("Params could be add to all entries\n");
  } else {
    printf("Params could NOT be added to all entries\n");
  }

  if (bls_cmdline_remove (all_cmdline, params_rem)) {
    printf("Params could be removed to all entries\n");
  } else {
    printf("Params could NOT be removed to all entries\n");
  }

  if (bls_cmdline_remove (all_cmdline, params_clear)) {
    printf("Params could be cleared to all entries\n");
  } else {
    printf("Params could NOT be cleared to all entries\n");
  }

  bls_entry_list_destroy (all);

  return 0;
}
